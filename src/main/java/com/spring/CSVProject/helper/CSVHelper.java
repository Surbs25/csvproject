package com.spring.CSVProject.helper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.springframework.web.multipart.MultipartFile;

import com.spring.CSVProject.model.CSVData;

public class CSVHelper {
  public static String TYPE = "text/csv";
  static String[] HEADERs = { "PersonId", "Name","Description", "UpdatedTimeStamp" };

  public static boolean hasCSVFormat(MultipartFile file) {

    if (!TYPE.equals(file.getContentType())) {
      return false;
    }

    return true;
  }

  public static List<CSVData> csvToData(InputStream is) {
    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        CSVParser csvParser = new CSVParser(fileReader,
            CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

      List<CSVData> csvdatarecords = new ArrayList<CSVData>();

      Iterable<CSVRecord> csvRecords = csvParser.getRecords();

      for (CSVRecord csvRecord : csvRecords) {
        CSVData csvdata = new CSVData(
              Long.parseLong(csvRecord.get("PersonId")),
              csvRecord.get("Name"),
              csvRecord.get("Description"),
             csvRecord.get("UpdatedTimeStamp")
            );

        csvdatarecords.add(csvdata);
      }

      return csvdatarecords;
    } catch (IOException e) {
      throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
    }
  }

  public static ByteArrayInputStream DataToCSV(List<CSVData> csvdatarecords) {
    final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
        CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);) {
      for (CSVData csvdata : csvdatarecords) {
        List<String> data = Arrays.asList(
              String.valueOf(csvdata.getPersonId()),
              csvdata.getName(),
              csvdata.getDescription(),
              String.valueOf(csvdata.getUpdatedTimeStamp())
            );

        csvPrinter.printRecord(data);
      }

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
    }
  }

}
