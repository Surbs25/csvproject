package com.spring.CSVProject.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.spring.CSVProject.helper.CSVHelper;
import com.spring.CSVProject.model.CSVData;
import com.spring.CSVProject.repository.CSVDataRepository;

@Service
public class CSVService {
  @Autowired
  private CSVDataRepository repository;

 public void save(MultipartFile file) {
    try {
      List<CSVData> csvdatarecords = CSVHelper.csvToTutorials(file.getInputStream());
      repository.saveAll(csvdatarecords);
    } catch (IOException e) {
      throw new RuntimeException("fail to store csv data: " + e.getMessage());
    }
  }

  public ByteArrayInputStream load() {
    List<CSVData> tutorials = repository.findAll();

    ByteArrayInputStream in = CSVHelper.DataToCSV(tutorials);
    return in;
  }

  public List<CSVData> getAll() {
    return repository.findAll();
  }
  public Long count() {

      return repository.count();
  }

  public  List<CSVData> deletedataById(Long personId) {

	  repository.deleteById(personId);
  }
  

  
}
