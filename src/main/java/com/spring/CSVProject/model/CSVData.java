package com.spring.CSVProject.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "person_data")
public class CSVData {

  @Id
  @Column(name = "PersonId")
  private long personId;

  @Column(name = "Name")
  private String name;

  public long getPersonId() {
	return personId;
}

public void setPersonId(long personId) {
	this.personId = personId;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public String getUpdatedTimeStamp() {
	return updatedTimeStamp;
}

public void setUpdatedTimeStamp(String updatedTimeStamp) {
	this.updatedTimeStamp = updatedTimeStamp;
}


@Column(name = "Description")
  private String description;

  @Column(name = "UpdatedTimeStamp")
  private String updatedTimeStamp;

  public CSVData() {

  }

  public CSVData(long personId, String name, String description, String updatedTimeStamp) {
    this.personId = personId;
    this.name = name;
    this.description = description;
    this.updatedTimeStamp = updatedTimeStamp;
  }

  
  @Override
  public String toString() {
    return "Tutorial [PersonId=" + personId + ", Name=" + name + ", Description=" + description + ", UpdatedTimeStamp=" + updatedTimeStamp + "]";
  }

}
