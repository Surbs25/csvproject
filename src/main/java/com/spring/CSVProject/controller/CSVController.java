package com.spring.CSVProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.spring.CSVProject.service.CSVService;
import com.spring.CSVProject.helper.CSVHelper;
import com.spring.CSVProject.message.ResponseMessage;
import com.spring.CSVProject.model.CSVData;

@CrossOrigin("http://localhost:8081")
@Controller
@RequestMapping("/api/csv")
public class CSVController {

  @Autowired
  com.spring.CSVProject.service.CSVService fileService;
  

  @PostMapping("/upload")
  public ResponseEntity<com.spring.CSVProject.message.ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
    String message = "";

    if (com.spring.CSVProject.helper.CSVHelper.hasCSVFormat(file)) {
      try {
        fileService.save(file);

        message = "Uploaded the file successfully: " + file.getOriginalFilename();
        return ResponseEntity.status(HttpStatus.OK).body(new com.spring.CSVProject.message.ResponseMessage(message));
      } catch (Exception e) {
        message = "Could not upload the file: " + file.getOriginalFilename() + "!";
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new com.spring.CSVProject.message.ResponseMessage(message));
      }
    }

    message = "Please upload a csv file!";
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new com.spring.CSVProject.message.ResponseMessage(message));
  }

  @GetMapping("/fetchCSVData")
  public ResponseEntity<List<com.spring.CSVProject.model.CSVData>> getAllData() {
    try {
      List<CSVData> data = fileService.getAll();

      if (data.isEmpty()) {
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      }

      return new ResponseEntity<>(data, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @PostMapping("/delete")
	    public ResponseEntity<List<CSVData>> delete(@PathVariable String id) {

	        Long userId = Long.parseLong(id);
	        List<CSVData> data = fileService.getAll();
	        		if (!data.isEmpty()) {
	        			data = fileService.deletedataById(userId);
	        			return new ResponseEntity<>(data, HttpStatus.OK);
	        	      }
					return new ResponseEntity<>(data, HttpStatus.GONE)  ;
	    }
  
}
