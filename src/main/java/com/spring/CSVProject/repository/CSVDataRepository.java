package com.spring.CSVProject.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.spring.CSVProject.model.CSVData;

public interface CSVDataRepository extends JpaRepository<CSVData, Long> {
}
